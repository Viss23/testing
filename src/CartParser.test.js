import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	describe('Validation', () => {
		it('incorrect data should return at least 1 error', () => {
			const validationError = parser.validate(
				`Prodyct name,Prce,Quantity
				Fettuccine,12.12,1
				Linguine,-5,2
				Taglierini,7.7,3
				Tripoline,6.6,8`);

			expect(validationError.length).toBeGreaterThan(0);
		})

		it('incorrect header in csv file should return object with header error on it', () => {
			const validationError = parser.validate(
				`Prodyct name,Price,Quantity
				Fettuccine,12.12,1
				Linguine,8.8,2
				Taglierini,7.7,3
				Tripoline,6.6,8`);

			const errorObject = {
				type: 'header',
				row: 0,
				column: 0,
				message: 'Expected header to be named "Product name" but received Prodyct name.',
			}

			expect(validationError).toContainEqual(errorObject);
		})

		it('incorrect count of values in row should return object with row error on it ', () => {
			const validationError = parser.validate(
				`Product name,Price,Quantity
			Fettuccine,12.12,1
			Linguine,8.8,2
			Taglierini
			Tripoline,6.6,8`);

			const errorObject = {
				type: 'row',
				row: 3,
				column: -1,
				message: 'Expected row to have 3 cells but received 1.',
			}

			expect(validationError).toContainEqual(errorObject);
		})

		it('empty string in cell with key name should return object with error message: nonempty string expected', () => {
			const validationError = parser.validate(
				`Product name,Price,Quantity
			Fettuccine,12.12,1
			Linguine,8.8,2
			,23,2
			Tripoline,6.6,8`);

			const errorObject = {
				type: 'cell',
				row: 3,
				column: 0,
				message: 'Expected cell to be a nonempty string but received "".',
			}

			expect(validationError).toContainEqual(errorObject);
		})

		it('non positive number in cell with key price should return object with error message: positive number expected', () => {
			const validationError1 = parser.validate(
				`Product name,Price,Quantity
				Fettuccine,12.12,1
				Linguine,8.8,2
				Taglierini,-5,2
				Tripoline,6.6,8`);

			const validationError2 = parser.validate(
				`Product name,Price,Quantity
				Fettuccine,'12.12',1`);

			const errorObject1 = {
				type: 'cell',
				row: 3,
				column: 1,
				message: 'Expected cell to be a positive number but received "-5".',
			}

			const errorObject2 = {
				type: 'cell',
				row: 1,
				column: 1,
				message: `Expected cell to be a positive number but received "'12.12'".`,
			}

			expect(validationError1).toContainEqual(errorObject1);
			expect(validationError2).toContainEqual(errorObject2);
		})
		it('correct data should not occur any validations errors', () => {
			const validationError = parser.validate(
				`Product name,Price,Quantity
				Fettuccine,12.12,1
				Linguine,8.8,2
				Taglierini,5,2
				Tripoline,6.6,8`
			)

			expect(validationError).toHaveLength(0);
		})
	})

	describe('Calculate total', () => {
		it('should return sum of price multiplied on quantity of all items', () => {
			const mockData = [
				{ name: 'Fettuccine', price: 5.5, quantity: 3 },
				{ name: 'Linguine', price: 6.5, quantity: 1 },
				{ name: 'spaghetti', price: 4, quantity: 2 }
			];

			const calculatedTotal = parser.calcTotal(mockData);
			expect(calculatedTotal).toEqual(31);
		})
	})

	describe('Create error', () => {
		it('create errors should return obj with expected types', () => {
			const expectedErrorObj = {
				type: 'cell',
				row: 1,
				column: 2,
				message: 'some error message'
			}

			const error = parser.createError('cell', 1, 2, 'some error message');
			const typeErrorsValues = Object.values(parser.ErrorType);

			expect(typeErrorsValues).toContain(error.type);
			expect(error).toHaveProperty('row', expect.any(Number));
			expect(error).toHaveProperty('column', expect.any(Number));
			expect(error).toHaveProperty('message', expect.any(String));
			expect(error).toEqual(expectedErrorObj);
		})

		describe('Parse', () => {
			it('validations errors should be written in console and thrown an error', () => {
				const consoleSpy = jest.spyOn(console, 'error');
				parser.readFile = jest.fn(
					() => `
					Product name,Price,Quantity
				Fettuccine,12.12,1
				Linguine,-8.8,2		
				`);

				expect(() => parser.parse('mock_path')).toThrowError(
					'Validation failed!'
				);

				expect(consoleSpy).toHaveBeenCalled();
			})

			it('should correcty parse string line to the object ', () => {
				const parsedObj = parser.parseLine('Fettuccine, 12.12, 1');
				const expectedObject = {
					name: 'Fettuccine',
					price: 12.12,
					quantity: 1
				}

				expect(parsedObj).toHaveProperty('name', expect.any(String));
				expect(parsedObj).toHaveProperty('price', expect.any(Number));
				expect(parsedObj).toHaveProperty('quantity', expect.any(Number));
				expect(parsedObj).toHaveProperty('id', expect.any(String));
				expect(parsedObj).toMatchObject(expectedObject);
			})

			it('parse line should add random id to returned result', () =>{
				const parsedObj = parser.parseLine('Fettuccine, 12.12, 1');

				expect(parsedObj).toHaveProperty('id', expect.any(String));
				expect(parsedObj.id).toHaveLength(36);
			})
		})
	})
});

describe('CartParser - integration test', () => {
	it('result of parsing csv should be array and total as a number', () =>{
		const parsedCSV = parser.parse('samples/cart.csv');

		expect(parsedCSV).toHaveProperty('items', expect.any(Array));
		expect(parsedCSV).toHaveProperty('total', expect.any(Number));
	})
});